import { Component } from "react";

class LuckyDraw extends Component {
    constructor() {
        super();
        this.state = {
            ball_1: "0",
            ball_2: "0",
            ball_3: "0",
            ball_4: "0",
            ball_5: "0",
        }
    }
    onGenerateClick =() => {
        this.setState({
            ball_1: Math.floor(Math.random() * 99 + 1),
            ball_2: Math.floor(Math.random() * 99 + 1),
            ball_3: Math.floor(Math.random() * 99 + 1),
            ball_4: Math.floor(Math.random() * 99 + 1),
            ball_5: Math.floor(Math.random() * 99 + 1),
        })
    }
    render() {
        
        return (
            <>
                <h1>Lucky Draw</h1>
                <div className="ball-row mt-4">
                    <div className="ball">{this.state.ball_1 = this.state.ball_1 < 10 ? '0' + this.state.ball_1 : this.state.ball_1}</div>
                    <div className="ball">{this.state.ball_2 = this.state.ball_2 < 10 ? '0' + this.state.ball_2 : this.state.ball_2}</div>
                    <div className="ball">{this.state.ball_3 = this.state.ball_3 < 10 ? '0' + this.state.ball_3 : this.state.ball_3}</div>
                    <div className="ball">{this.state.ball_4 = this.state.ball_4 < 10 ? '0' + this.state.ball_4 : this.state.ball_4}</div>
                    <div className="ball">{this.state.ball_5 = this.state.ball_5 < 10 ? '0' + this.state.ball_5 : this.state.ball_5}</div>
                </div>
                <button className="btn btn-success mt-4" onClick={this.onGenerateClick}>Generate</button>
            </>
        )
    }
}

export default LuckyDraw