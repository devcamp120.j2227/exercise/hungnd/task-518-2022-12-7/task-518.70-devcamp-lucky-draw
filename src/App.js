import 'bootstrap/dist/css/bootstrap.min.css';

import './App.css';

import LuckyDraw from './components/LuckyDraw';

function App() {
  return (
    <div className='container text-center mt-5'>
      <LuckyDraw/>
    </div>
  );
}

export default App;
